#!/bin/bash 
if [ -e "php-cs-fixer.phar" ]
then
	php php-cs-fixer.phar self-update
else
	wget http://cs.sensiolabs.org/get/php-cs-fixer.phar	
fi
if [ -e "mageekguy.atoum.phar" ]
then
	php -d phar.readonly=0 mageekguy.atoum.phar --update
else
	wget http://downloads.atoum.org/nightly/mageekguy.atoum.phar	
fi

php php-cs-fixer.phar fix tests/units
php composer.phar update
php mageekguy.atoum.phar -c tests/configurations/coverage.php -d tests/units
