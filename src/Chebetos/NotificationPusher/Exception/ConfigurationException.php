<?php

namespace Chebetos\NotificationPusher\Exception;

use Chebetos\NotificationPusher\Exception\Exception;

/**
 * ConfigurationException.
 *
 * @uses InvalidArgumentException
 * @author Cédric Dugat <ph3@slynett.com>
 */
class ConfigurationException extends \InvalidArgumentException implements Exception
{
}
