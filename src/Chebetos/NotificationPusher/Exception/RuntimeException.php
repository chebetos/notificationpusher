<?php

namespace Chebetos\NotificationPusher\Exception;

use Chebetos\NotificationPusher\Exception\Exception;

/**
 * RuntimeException.
 *
 * @uses RuntimeException
 * @author Cédric Dugat <ph3@slynett.com>
 */
class RuntimeException extends \RuntimeException implements Exception
{
}
